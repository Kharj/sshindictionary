package com.kharj.sshindictionary;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView.OnEditorActionListener;

public class MainActivity extends Activity implements OnItemSelectedListener, View.OnClickListener, TextWatcher {

	public static final String TAG = "sshindictionary";
	protected EditText inputWord;
	protected EditText resultWord;
	protected EditText prefixText;
	protected EditText baseText;
	protected EditText suffixText;
	protected Spinner numberSpinner;
	protected Spinner genderSpinner;
	protected Spinner caseSpinner;
	protected TextView caseText;
	protected Button clearButton;
	protected Button saveButton;
	protected DBHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		inputWord = (EditText)findViewById(R.id.input_word);
		resultWord = (EditText)findViewById(R.id.result_word);
		prefixText = (EditText)findViewById(R.id.input_prefix);
		baseText = (EditText)findViewById(R.id.input_base);
		suffixText = (EditText)findViewById(R.id.input_suffix);
		numberSpinner = (Spinner)findViewById(R.id.number_spinner);
		genderSpinner = (Spinner)findViewById(R.id.gender_spinner);
		caseSpinner = (Spinner)findViewById(R.id.case_spinner);
		clearButton = (Button)findViewById(R.id.clear_button);
		saveButton = (Button)findViewById(R.id.save_button);
		caseText = (TextView)findViewById(R.id.case_text);
		caseSpinner.setVisibility(View.GONE);
		caseText.setVisibility(View.GONE);

		numberSpinner.setOnItemSelectedListener(this);
		genderSpinner.setOnItemSelectedListener(this);
		caseSpinner.setOnItemSelectedListener(this);
		clearButton.setOnClickListener(this);
		saveButton.setOnClickListener(this);
		inputWord.addTextChangedListener(this);

		setNumberAvailability(false, false, false);
		setGenderAvailability(false, false, false, false);
		dbHelper = new DBHelper(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {

			Toast.makeText(getBaseContext(), "Лаб.работа студента группы ИНФ-11-1 Харченко Е.Д.\nevgen.kharchenko.ua@gmail.com", Toast.LENGTH_SHORT).show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if(parent.getId()==numberSpinner.getId() && position==2){
			genderSpinner.setSelection(0);
		}
		searchWord(inputWord.getText().toString(), numberSpinner.getSelectedItemPosition(), genderSpinner.getSelectedItemPosition());
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0.getId()==clearButton.getId()){
			inputWord.setText("");
			resultWord.setText("");
			setNumberAvailability(false, false, false);
			setGenderAvailability(false, false, false, false);
		}else if(arg0.getId()==saveButton.getId()){
			saveWord(prefixText.getText().toString(), baseText.getText().toString(), suffixText.getText().toString());
		}
	}

	protected void setResultText(String res){
		if(res==null){
			resultWord.setTextColor(Color.RED);
			resultWord.setText("Не найдено");
			//show add sialog
		}else{
			resultWord.setTextColor(Color.BLACK);
			resultWord.setText(res);
		}
	}
	protected void setNumberAvailability(boolean no, boolean single, boolean multiple){
		String [] array_spinner=new String[3];
		array_spinner[0]=((no)?"":"-")+"Не выбрано";
		array_spinner[1]=((single)?"":"-")+"Единственное";
		array_spinner[2]=((multiple)?"":"-")+"Множественное";
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, array_spinner);
		numberSpinner.setAdapter(adapter);
		if(multiple) numberSpinner.setSelection(2);
		if(single) numberSpinner.setSelection(1);
		if(no) numberSpinner.setSelection(0);
	}

	protected void setGenderAvailability(boolean no, boolean m, boolean f, boolean mid){
		String [] array_spinner=new String[4];
		array_spinner[0]=((no)?"":"-")+"Не выбрано";
		array_spinner[1]=((m)?"":"-")+"Мужской";
		array_spinner[2]=((f)?"":"-")+"Женский";
		array_spinner[3]=((mid)?"":"-")+"Средний";
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, array_spinner);
		genderSpinner.setAdapter(adapter);
		if(mid) genderSpinner.setSelection(3);
		if(f) genderSpinner.setSelection(2);
		if(m) genderSpinner.setSelection(1);
		if(no) genderSpinner.setSelection(0);
	}

	@Override
	public void afterTextChanged(Editable s) {
		searchWord(s.toString(), null, null);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}
	protected String safeString(String s){
		if(s==null) return null;
		String splitarr[] = s.split("[\\.,?! ]+", 2);
		return splitarr[0].toLowerCase();
	}
	protected void searchWord(String word, Integer number, Integer gender){
		word = safeString(word);
		Log.i(TAG, "word= "+word);
		String res = null;
		if(word != null && !word.isEmpty()){
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			// проверка существования записей
			String table = "modword as mw inner join baseword as bw on  mw.base_id=bw.id";
			String[] params = new String[]{"bw.id as id", "bw.word as base", "mw.suffix", "mw.prefix",
					"mw.number", "mw.gender", "mw.tense", "mw.prefix||bw.word||mw.suffix as fullword"};
			String where = "fullword=?";
			String[] values = new String[]{word};
			String limit = "1";
			if(number==null && gender==null){
				//set spinners
				setNumberAvailability(false, false, false);
				setGenderAvailability(false, false, false, false);

			}
			Cursor c = db.query(table, params, where, values, null, null, null, limit);
			if (c.getCount() != 0 && c.moveToFirst()) {
				int _number = c.getInt(c.getColumnIndex("number"));
				int _gender = c.getInt(c.getColumnIndex("gender"));
				//int _tense = c.getInt(c.getColumnIndex("tense"));
				int baseid=c.getInt(c.getColumnIndex("id"));
				if(number==null && gender==null){
					c.close();
					//set spinners
					where = "id=?";
					values = new String[]{Integer.toString(baseid)};
					limit = null;
					c = db.query(table, params, where, values, null, null, null, limit);
					if(c.getCount()!=0){
						boolean isNoNumber = false;
						boolean isNoGender = false;
						boolean isMale = false;
						boolean isFemale = false;
						boolean isMid = false;
						boolean isSingle = false;
						boolean isMultiple = false;
						while(c.moveToNext()){
							if(c.getInt(c.getColumnIndex("number"))==0) isNoNumber = true;
							if(c.getInt(c.getColumnIndex("gender"))==0) isNoGender = true;
							if(c.getInt(c.getColumnIndex("number"))==1) isSingle = true;
							if(c.getInt(c.getColumnIndex("number"))==2) isMultiple = true;
							if(c.getInt(c.getColumnIndex("gender"))==1) isMale = true;
							if(c.getInt(c.getColumnIndex("gender"))==2) isFemale = true;
							if(c.getInt(c.getColumnIndex("gender"))==3) isMid = true;
						}
						setNumberAvailability(isNoNumber, isSingle, isMultiple);
						setGenderAvailability(isNoGender, isMale, isFemale, isMid);
						numberSpinner.setSelection(_number);
						genderSpinner.setSelection(_gender);
					}
				}else{//(number==null && gender==null)
					c.close();
					where = "id=? AND number=? AND gender=?";
					values = new String[]{Integer.toString(baseid), Integer.toString(number), Integer.toString(gender)};
					limit = "1";
					c = db.query(table, params, where, values, null, null, null, limit);
					if (c.getCount() != 0 && c.moveToFirst()) {
						//Yay
						res = c.getString(c.getColumnIndex("fullword"));
						prefixText.setText(c.getString(c.getColumnIndex("prefix")));
						suffixText.setText(c.getString(c.getColumnIndex("suffix")));
						baseText.setText(c.getString(c.getColumnIndex("base")));
					}
				}
			}else{//getcount

			}
			c.close();
			dbHelper.close();
		}
		if(res==null){
			prefixText.setText("");
			suffixText.setText("");
			baseText.setText(word);
			//show change dialog
		}else{
			//hide change dialod
		}
		setResultText(res);
	}
	protected void saveWord(String prefix, String base, String suffix){
		if(base==null || base.isEmpty()){
			return;
		}
		prefix = safeString(prefix);
		base = safeString(base);
		suffix = safeString(suffix);
		
		int number = numberSpinner.getSelectedItemPosition();
		int gender = genderSpinner.getSelectedItemPosition();
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		// проверка существования записей
		String table = "modword as mw inner join baseword as bw on  mw.base_id=bw.id";
		String[] params = new String[]{"bw.id as id", "bw.word as base", "mw.suffix", "mw.prefix",
				"mw.number", "mw.gender", "mw.tense", "mw.prefix||bw.word||mw.suffix as fullword"};
		String where = "base=? AND number=? AND gender=?";
		String[] values = new String[]{base, Integer.toString(number), Integer.toString(gender)};
		String limit = "1";
		Cursor c = db.query(table, params, where, values, null, null, null, limit);
		if (c.getCount() != 0 && c.moveToFirst()) {
			//found
			//update pref/suf
			table = "modword";
			int baseid=c.getInt(c.getColumnIndex("id"));
			params = new String[]{"id", "word"};
			where = "base_id=? AND number=? AND gender=?";
			values = new String[]{Integer.toString(baseid), Integer.toString(number), Integer.toString(gender)};
		    ContentValues cv = new ContentValues();
		    cv.put("prefix", prefix);
		    cv.put("suffix", suffix);
		    
			int updated = db.update(table, cv ,where , values);
			if(updated>0){
				Toast.makeText(getBaseContext(), "Сохранено", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(getBaseContext(), "Ошибка сохранения", Toast.LENGTH_SHORT).show();
			}
		}else{
			//mod not found, search base
			table = "baseword";
			params = new String[]{"id", "word"};
			where = "word=?";
			values = new String[]{base};
			limit = "1";

			int baseid=-1;
			c.close();
			c = db.query(table, params, where, values, null, null, null, limit);
			if (c.getCount() != 0 && c.moveToFirst()) {
				baseid = c.getInt(c.getColumnIndex("id"));
			}else{
				//create base
				ContentValues cv = new ContentValues();
			    cv.put("word", base);
				long updres = db.insert("baseword", null, cv);
				if(updres>0){
					table = "baseword";
					params = new String[]{"id", "word"};
					where = "word=?";
					values = new String[]{base};
					limit = "1";
					c.close();
					c = db.query(table, params, where, values, null, null, null, limit);
					if (c.getCount() != 0 && c.moveToFirst()) {
						baseid = c.getInt(c.getColumnIndex("id"));
					}
				}
			}
			//create mod
			if(baseid==-1){
				Toast.makeText(getBaseContext(), "Ошибка сохранения", Toast.LENGTH_SHORT).show();
			}else{
				ContentValues cv = new ContentValues();
			    cv.put("base_id", baseid);
			    cv.put("prefix", prefix);
			    cv.put("suffix", suffix);
			    cv.put("number", number);
			    cv.put("gender", gender);
			    cv.put("tense", 0);//
				long updres = db.insert("modword", null, cv);
				if(updres>0){
					Toast.makeText(getBaseContext(), "Сохранено", Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(getBaseContext(), "Ошибка сохранения", Toast.LENGTH_SHORT).show();
				}
			}
		}
		c.close();
	}
}
