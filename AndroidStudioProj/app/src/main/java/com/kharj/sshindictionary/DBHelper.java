package com.kharj.sshindictionary;

import android.content.Context;
import android.database.sqlite.*;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	final static String DB_NAME = "sshindictionary"; // имя БД
	final static int DB_VERSION = 12; // версия БД
	public static final String TAG = "sshindictionary";
	public DBHelper(Context context) {
		// конструктор суперкласса
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(TAG, "onCreate database");
		// создаем таблицу с полями
		try{    		
			db.execSQL("create table baseword ("
					+ "id integer primary key autoincrement," 
					+ "word text);" );
			db.execSQL("create table modword ("
					+ "base_id integer,"
					+ "prefix text,"
					+ "suffix text,"
					+ "number integer,"
					+ "gender integer,"
					+ "tense integer,"
					+ "FOREIGN KEY(base_id) REFERENCES baseword(id),"
					+ "primary key (base_id, number, gender, tense));" );
			db.execSQL("insert into baseword (word) values ('умн');" );
			db.execSQL("insert into modword (prefix, base_id, suffix, number, gender, tense) values ('', (SELECT id FROM baseword WHERE word='умн'), 'ый', 1, 1, 0);" );
			db.execSQL("insert into modword (prefix, base_id, suffix, number, gender, tense) values ('', (SELECT id FROM baseword WHERE word='умн'), 'ая', 1, 2, 0);" );
			db.execSQL("insert into modword (prefix, base_id, suffix, number, gender, tense) values ('', (SELECT id FROM baseword WHERE word='умн'), 'ое', 1, 3, 0);" );
			db.execSQL("insert into modword (prefix, base_id, suffix, number, gender, tense) values ('', (SELECT id FROM baseword WHERE word='умн'), 'ые', 2, 0, 0);" );
			}catch(SQLiteException ex){
			Log.d(TAG, "onCreate db error ");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion<newVersion){
			Log.d(TAG, "onUpadate database");
			try{    		
				db.execSQL("drop table baseword;");
			}catch(SQLiteException ex){
				Log.d(TAG, "onUpadate db error ");
			}
			try{    		
				db.execSQL("drop table modword;");
			}catch(SQLiteException ex){
				Log.d(TAG, "onUpadate db error ");
			}
			onCreate(db);
		}
	}

}